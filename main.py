#!/usr/bin/python

# ZetCode PyGTK tutorial 
#
# This is a trivial PyGTK example
#
# author: jan bodnar
# website: zetcode.com 
# last edited: February 2009

'''
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

window = Gtk.Window(title="Hello World")
window.show()
window.connect("destroy", Gtk.main_quit)
Gtk.main()
'''

import os
from threading import Timer
from playsound import playsound
#from pydub import AudioSegment
#from pydub.playback import play
#import simpleaudio as sa

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk as gtk

#class App(gtk.Window):
#    def __init__(self):
#        super(PyApp, self).__init__()
#        
#        self.connect("destroy", gtk.main_quit)
#        self.set_size_request(250, 150)
#        #self.set_position(gtk.GTK_WIN_POS_CENTER)
#        self.show()

template = """\
<?xml version="1.0" encoding="UTF-8"?>
<interface>
  <!-- interface-requires gtk+ 3.0 -->
  <object class="GtkWindow" id="window1">
    <property name="can_focus">False</property>
    <child>
      <object class="GtkButton" id="button1">
        <property name="label" translatable="yes">button</property>
        <property name="use-action-appearance">False</property>
        <property name="visible">True</property>
        <property name="can-focus">True</property>
        <property name="receives-default">True</property>
      </object>
    </child>
  </object>
</interface>
"""

finish = 0

class App(gtk.Window):
    __gtype_name__ = "window1"

    def __init__(self):
        #self.builder = gtk.Builder.new_from_string(template, -1)
        self.builder = gtk.Builder.new_from_file(os.getcwd() + "/main_ui.glade")
        self.builder.connect_signals(self)
        self.window = self.builder.get_object("wMain")
        self.label  = self.builder.get_object("txTime")
        self.menu   = self.builder.get_object("lbList")
        self.window.connect("destroy", gtk.main_quit)

        self.timer = Timer(1, self.time_event)
        self.mins = self.secs = 0
        self.status = 0
        self.delay = 1
        self.count = 1

    def hello_button_clicked(self, *args):
      pass

    def btn_start_click(self, *args):
      if self.status == 1:
        self.status = 0
        self.timer.cancel()

      self.mins = self.secs = 0
      self.timer = Timer(1, self.time_event)
      self.timer.start()
      playsound(os.getcwd() + '/start.wav')
      self.status = 1
      self.count = self.delay
      pass

    def lbox_list_change(self, *args):
      print("list box change " + str(args))
      self.mins = self.secs = 0
      #self.timer = Timer(1, self.time_event)
      #self.timer.start()
      #playsound(os.getcwd() + '/start.wav')
      #self.status = 1
      id = self.menu.get_active_text()
      print("list box change id is " + str(id))

      if   id == "1:1":
        self.delay = 1
      elif id == "2:1":
        self.delay = 2
      elif id == "3:1":
        self.delay = 3
      elif id == "5:1":
        self.delay = 5

      #self.count = self.delay
      

    def time_event(self):
      if finish == 1:
        self.timer.cancel()
        return

      if self.secs < 59:
        self.secs += 1
      else:
        self.mins += 1
        self.secs = 0

        self.count = self.count - 1
        
        print("Timer count: " + str(self.count))

        if self.count <= 0:
          if self.status == 1:
            self.status = 0
            self.count = 1
            playsound(os.getcwd() + '/pause.wav')
          else:
            self.status = 1
            self.count = self.delay
            playsound(os.getcwd() + '/start.wav')
      
      self.label.set_text(str(self.mins) + ":" + str(self.secs))
      print("Timer: " + str(self.mins) + ":" + str(self.secs) + " count: " + str(self.count))

      self.timer.cancel()
      self.timer = Timer(1, self.time_event)
      self.timer.start()

    def show(self):
        self.window.show_all()

app = App()
app.show()
gtk.main()
finish = 1
